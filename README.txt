This module differs from this one: http://drupal.org/project/search_api_et. It 
uses an alternative way of getting language aware search with entity translation: 
have one index per language. This has some drawbacks like scalability and site 
building usability. But when used with Solr as indexing backend, it allows to set 
a different configuration for each language (by declaring one Search API server 
per language, matching to a different Solr core). This especially makes sense if 
the website only has two or three languages.
For more info on these considerations, see http://drupal.org/node/1393058.
