<?php

class EntityTranslationDataSourceController extends SearchApiEntityDataSourceController {

  /**
   * Get a metadata wrapper for the item type of this data source controller.
   *
   * @param $item
   *   Unless NULL, an item of the item type for this controller to be wrapped.
   * @param array $info
   *   Optionally, additional information that should be used for creating the
   *   wrapper. Uses the same format as entity_metadata_wrapper().
   *
   * @return EntityMetadataWrapper
   *   A wrapper for the item type of this data source controller, according to
   *   the info array, and optionally loaded with the given data.
   *
   * @see entity_metadata_wrapper()
   */
  public function getMetadataWrapper($item = NULL, array $info = array()) {
    if (is_array($info['property info alter'])) {
      $index = reset($info['property info alter']);
      $target_language = $index->options['entity_translation_language'];
      if ($target_language != ENTITY_TRANSLATION_SEARCH_API_NO_TRANSLATION) {
        // This may not be really needed.
        $info['langcode'] = $target_language;
        $wrapper = parent::getMetadataWrapper($item, $info);
        $wrapper->language($target_language);
        return $wrapper;
      }
    }
    return parent::getMetadataWrapper($item, $info);
  }

}
